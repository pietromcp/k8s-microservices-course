#!/usr/bin/env bash
if [ $# -eq 2 -a "$1" == "-n" ] ; then
  ns=$2
else
  ns=$(kubectl config view --minify --output 'jsonpath={..namespace}'; echo)
fi

for pod in $(kubectl get pods -n $ns | grep -v 'STATUS' | tr -s ' ' | cut -d ' ' -f 1)
do
  kubectl describe pod $pod -n $ns | grep '^Node:' | tr -s ' '
done
