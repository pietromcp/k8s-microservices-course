#!/bin/bash
echo "0"
if [ $# -ne 1 ] ; then
  echo "Usage: ./stop-pen.sh <PORT>" >&2
  exit 1
fi
echo "1"
pid=$(ps aux | grep pen | grep 0.0.0.0:$1 | tr -s ' ' | cut -d ' ' -f 2)
echo "Stop pen process with id ${pid}? [yN]"
read answer
if [ "y" == "$(echo ${answer} | tr [A-Z] [a-z])" ] ; then
  kill -9 ${pid}
else
  echo "Process not killed"
fi
