#!/bin/bash
if [ $# -ne 1 ] ; then
  echo "Usage: ./start-pen.sh <PORT>" >&2 
  exit 1
fi
pen 0.0.0.0:$1 172.29.19.10:80 172.29.19.11:80 172.29.19.12:80
